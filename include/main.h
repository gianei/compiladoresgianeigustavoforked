/*
  main.h

  Cabeçalho principal do analisador sintático
*/
#ifndef __MAIN_H
#define __MAIN_H
#include <stdio.h>
#include "cc_dict.h"
#include "cc_list.h"
#include "cc_tree.h"
#include "parser.h"

/*
  Constantes a serem utilizadas como valor de retorno no caso de
  sucesso (SINTATICA_SUCESSO) e erro (SINTATICA_ERRO) do analisador
  sintático.
*/
#define SINTATICA_SUCESSO 0
#define SINTATICA_ERRO    1

#define IKS_SUCCESS 0 //caso n˜ao houver nenhum tipo de erro
/* Verificac¸˜ao de declarac¸˜oes */
#define IKS_ERROR_UNDECLARED 1 //identificador n˜ao declarado
#define IKS_ERROR_DECLARED 2 //identificador j´a declarado
/* Uso correto de identificadores */
#define IKS_ERROR_VARIABLE 3 //identificador deve ser utilizado como vari´avel
#define IKS_ERROR_VECTOR 4 //identificador deve ser utilizado como vetor
#define IKS_ERROR_FUNCTION 5 //identificador deve ser utilizado como func¸˜ao
/* Tipos e tamanho de dados */
#define IKS_ERROR_WRONG_TYPE 6 //tipos incompat´ıveis
#define IKS_ERROR_STRING_TO_X 7 //coerc¸˜ao imposs´ıvel do tipo string
#define IKS_ERROR_CHAR_TO_X 8 //coerc¸˜ao imposs´ıvel do tipo char
/* Argumentos e parˆametros */
#define IKS_ERROR_MISSING_ARGS 9 //faltam argumentos
#define IKS_ERROR_EXCESS_ARGS 10 //sobram argumentos
#define IKS_ERROR_WRONG_TYPE_ARGS 11 //argumentos incompat´ıveis
/* Verificac¸˜ao de tipos em comandos */
#define IKS_ERROR_WRONG_PAR_INPUT 12 //parˆametro n˜ao ´e identificador
#define IKS_ERROR_WRONG_PAR_OUTPUT 13 //parˆametro n˜ao ´e literal string ou express˜ao
#define IKS_ERROR_WRONG_PAR_RETURN 14 //parˆametro n˜ao ´e express˜ao compat´ıvel com tip

/*
  Constantes a serem utilizadas para diferenciar os lexemas que estão
  registrados na tabela de símbolos.
*/
#define SIMBOLO_LITERAL_INT    1
#define SIMBOLO_LITERAL_FLOAT  2
#define SIMBOLO_LITERAL_CHAR   3
#define SIMBOLO_LITERAL_STRING 4
#define SIMBOLO_LITERAL_BOOL   5
#define SIMBOLO_IDENTIFICADOR  6

void cc_dict_etapa_1_print_entrada (char *token, int line);

#endif
