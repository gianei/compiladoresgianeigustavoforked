#ifndef __MISC_H
#define __MISC_H
#include <stdio.h>
#include "cc_dict.h"
#include "cc_tree.h"
#include "cc_stack.h"

int getLineNumber (void);
void yyerror (char const *mensagem);
void main_init (int argc, char **argv);
void main_finalize (void);
void comp_print_table(void);

extern int myLineNumber;
extern comp_dict_t *dick;
extern comp_tree_t* tree;

//valor do temp atual
extern int global_id;
//usado na gera_rotulo
extern int rotulo_id;
//tipo da funcao atual
extern int func_type;
//pilha local
extern int fp;
//pilha global
extern int rbss;

#define DICT_TYPE_VAR 1
#define DICT_TYPE_FUNC 2
#define DICT_TYPE_VEC 3

extern int parserReturn;

typedef union typeValue{
  float f;
  int i;
  char c;
  char *s;
  int b;
} typeValue_t;


// 0 -> n modificar
// 1 -> modificar para INT
// 2 -> modificar para FLOAT
// 3 -> modificar para BOOL

typedef struct simbolo{
  int line;
  char *lexema;
  int mod; //modificar tipo na geracao de codigo
  int type; //tipo de literal
  int varType; //tipo da variavel
  int size;
  int isUserType; //se é um tipo de usuário
  int dictType;
  struct simbolo* userType; //qual é esse tipo de usuário
  typeValue_t value;
  stack_t *parameters;
  int escopo; //0 para escopo global, 1 escopo de função
  int desloc; //deslocamento a partir do escopo global (rbss) ou função (fp)
} simbolo_t;

typedef struct ast_nodo{
  int tipoNodo;
  int isIfWithElse;
  char* local;
  char* codigo;
  simbolo_t* simbolo;
} ast_nodo_t;


void* tableInsert(char *key, int type, int line);

//checa se o tipo atribuido é o mesmo que foi declarado, se não é: caso seja uma situação onde não se possa modificar o tipo -> erro; caso seja possível indica o no dicionário que na geração de código essa variável terá de ver modificada
//lexema = nome da variavel/vetor
//tipo da variavel quando declarada
//tipo da expressao que esta sendo atribuido
//elemento do dicionario que representa a expressao
//id = 0 -> variavel; 1 -> vetor (usado na hora do print); 2 -> funcao
void check_type(char* lexema, int type_decl, int type_atr, simbolo_t* target, int id);
char* gera_registrador();
int size_tipo(int tipo);

void parse_aritmetic(ast_nodo_t* n1, ast_nodo_t* n2, ast_nodo_t* n3);

#endif
