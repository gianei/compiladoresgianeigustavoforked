#ifndef CC_STACK_H_
#define CC_STACK_H_

//#define ERRO(MENSAGEM) { fprintf (stderr, "[cc_stack, %s] %s.\n", __FUNCTION__, MENSAGEM); abort(); }

typedef struct comp_stack {
	void *value;
	struct comp_stack *next,*prev;
} comp_stack_t;

typedef struct stack {
	comp_stack_t *current;
	comp_stack_t *bottom;
} stack_t;

stack_t* stack_new(void);
void stack_free(stack_t *stack);
comp_stack_t* stack_make_node(void *value);
void stack_push(stack_t *stack, comp_stack_t *node);
void stack_pop(stack_t *stack);
static void stack_debug_print_s(comp_stack_t *stack, int spacing);
void stack_debug_print(stack_t *stack);

extern stack_t* stack;
extern stack_t* parametersStack;
comp_dict_item_t* search_stack(stack_t *stack, char *key, int type);
comp_dict_item_t* search_stack_local(stack_t *stack, char *lexema, int type);

#endif
