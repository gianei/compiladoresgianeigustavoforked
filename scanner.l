/*
  Grupo 09

  Gianei Leandro Sebastiany
  Gustavo Laguna de Souza
*/

%x IN_COMMENT

%{
#include "cc_dict.h"
#include "cc_misc.h"
#include "main.h"
#include "parser.h" //arquivo automaticamente gerado pelo bison

int myLineNumber = 1;


%}


LETTER [a-zA-Z_]
DIGIT [0-9]
LETTERDIGIT [a-zA-Z0-9_]
    /* esses caracteres são usados principalmente quando reconhecendo char */
SPECIALCHAR [\(\)\[\]{}+-\\\\*,<>=!&$%#^]

%%

    /* estado padrão do flex */
<INITIAL>
{

    /* início de comentário */
"/*" BEGIN(IN_COMMENT);

    /* comentário de linha */
"//".*

    /* palavras reservadas */
int return TK_PR_INT;
float return TK_PR_FLOAT;
bool return TK_PR_BOOL;
char return TK_PR_CHAR;
string return TK_PR_STRING;
if return TK_PR_IF;
then return TK_PR_THEN;
else return TK_PR_ELSE;
while return TK_PR_WHILE;
do return TK_PR_DO;
input return TK_PR_INPUT;
output return TK_PR_OUTPUT;
return return TK_PR_RETURN;
const return TK_PR_CONST;
static return TK_PR_STATIC;
foreach return TK_PR_FOREACH;
for return TK_PR_FOR;
switch return TK_PR_SWITCH;
case return TK_PR_CASE;
break return TK_PR_BREAK;
continue return TK_PR_CONTINUE;
class return TK_PR_CLASS;
private return TK_PR_PRIVATE;
public return TK_PR_PUBLIC;
protected return TK_PR_PROTECTED;

false { yylval.valor_simbolo_lexico = tableInsert(yytext, SIMBOLO_LITERAL_BOOL, myLineNumber); return TK_LIT_FALSE;}
true { yylval.valor_simbolo_lexico = tableInsert(yytext, SIMBOLO_LITERAL_BOOL, myLineNumber); return TK_LIT_TRUE;}

    /* operadores compostos */
"<=" return TK_OC_LE;
">=" return TK_OC_GE;
"==" return TK_OC_EQ;
"!=" return TK_OC_NE;
"&&" return TK_OC_AND;
"||" return TK_OC_OR;
">>" return TK_OC_SR;
"<<" return TK_OC_SL;

    /* caracteres especiais */
"," return 44;
";" return 59;
":" return 58;
"(" return 40;
")" return 41;
"[" return 91;
"]" return 93;
"{" return 123;
"}" return 125;

    /* operadores */
"+" return 43;
"-" return 45;
"*" return 42;
"/" return 47;
"<" return 60;
">" return 62;
"=" return 61;
"!" return 33;
"&" return 38;
"$" return 36;
"%" return 37;
"#" return 35;
"^" return 94;

    /* literais */
{DIGIT}+"."{DIGIT}+ { yylval.valor_simbolo_lexico = tableInsert(yytext, SIMBOLO_LITERAL_FLOAT, myLineNumber); return TK_LIT_FLOAT;}

{DIGIT}+ { yylval.valor_simbolo_lexico = tableInsert(yytext, SIMBOLO_LITERAL_INT, myLineNumber); return TK_LIT_INT;}

'({LETTERDIGIT}|{SPECIALCHAR})' { yylval.valor_simbolo_lexico = tableInsert(yytext, SIMBOLO_LITERAL_CHAR, myLineNumber); return TK_LIT_CHAR;}

\"(\\.|[^\\"])*\" { yylval.valor_simbolo_lexico = tableInsert(yytext, SIMBOLO_LITERAL_STRING, myLineNumber); return TK_LIT_STRING;}

    /* identificadores */
{LETTER}{LETTERDIGIT}* { yylval.valor_simbolo_lexico = tableInsert(yytext, SIMBOLO_IDENTIFICADOR, myLineNumber); return TK_IDENTIFICADOR;}

    /* contador de linhas */
\n myLineNumber++;

    /* skip whitespaces */
" "

    /* skip tabs */
\t
\v
\f

    /* identificador não reconhecido retorna erro */
. return TOKEN_ERRO;

}

    /* em comentário */
<IN_COMMENT>
{
    "*/"        BEGIN(INITIAL);
    [^*\n]+
    "*"
    \n          myLineNumber++;
}


%%
