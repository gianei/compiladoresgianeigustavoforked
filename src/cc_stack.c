#include <main.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cc_stack.h"
#include "cc_dict.h"

#define ERRO(MENSAGEM) { fprintf (stderr, "[cc_dict, %s] %s.\n", __FUNCTION__, MENSAGEM); abort(); }

stack_t* stack_new(void){
	stack_t *stack = malloc(sizeof(stack_t));
	stack->current = NULL;
	stack->bottom = NULL;
	return stack;
}

void stack_free(stack_t *stack){
  comp_stack_t *ptr = stack->bottom;
  comp_stack_t *current = ptr;
	do {
		ptr = current->next;
		free(current);
		current = ptr;
	} while(ptr != NULL);
}

comp_stack_t* stack_make_node(void *value){
	comp_stack_t *node = malloc(sizeof(comp_stack_t));
	if (!node)
		ERRO("Failed to allocate memory for tree node");

	node->value = value;
	node->next = NULL;
	node->prev = NULL;
	return node;
}

void stack_push(stack_t *stack, comp_stack_t *node){
	if (stack == NULL)
		ERRO("Cannot insert node, stack is null");
	if (node == NULL)
		ERRO("Cannot insert node, node is null");

	if (stack->bottom == NULL){ //primeira vez
		stack->bottom = node;
    	stack->current = node;
	} else {
    	stack->current->next = node;
    	node->prev = stack->current;

    	stack->current = node;
	}

}

void stack_pop(stack_t *stack){
	if (stack == NULL)
		ERRO("Cannot pop node, stack is null");

  comp_stack_t *ptr = stack->current;

  stack->current = ptr->prev;
  if (stack->current != NULL)
    stack->current->next = NULL;

  //printf("\nPop from stack\n");
  //stack_debug_print_s(ptr, 0);

  free(ptr->value);
  free(ptr);

}

static void print_spaces(int num){
	while (num-->0)
		putc(' ',stdout);
}

static void stack_debug_print_node(comp_stack_t *stack, int spacing){
	if (stack == NULL) return;
	print_spaces(spacing);

	if (*((int*)stack->value) <= 6 && *((int*)stack->value) >= 0){ //not pointer to dict
		printf("ParmType: %d\n", *((int*)stack->value));
		return;
	}

  comp_dict_t* meuDick = (comp_dict_t*)stack->value;
  if (meuDick == NULL)
		printf("%p: %p\n",stack, meuDick);
	else
	    dict_debug_print (meuDick);
}

static void stack_debug_print_s(comp_stack_t *stack, int spacing){
	if (stack == NULL) return;
	do {
		stack_debug_print_node(stack, spacing);

		stack = stack->next;
	} while(stack != NULL);
}


void stack_debug_print(stack_t *stack){
  printf("\nPrinting stack %p\n",stack);
	stack_debug_print_s(stack->bottom, 0);
        //tree_free(tree);
}

comp_dict_item_t* search_stack(stack_t *stack, char *lexema, int type)
{
	char* key = malloc(strlen(lexema)+2);
	char tp[10]; sprintf(tp, "%d", type);
  	strcpy(key, tp); strcat(key, lexema);
	//printf("--------%10s %5d-----------\n", lexema, type);

	comp_stack_t *start = stack->current;
	comp_dict_item_t* target = NULL;

	while(stack->current!=NULL && target==NULL)
	{
		comp_stack_t *ptr = stack->current;
		target = dict_get2(ptr->value, key);
		stack->current = ptr->prev;
	}

	stack->current = start;

	return target;
}

comp_dict_item_t* search_stack_local(stack_t *stack, char *lexema, int type)
{
	char* key = malloc(strlen(lexema)+2);
	char tp[10]; sprintf(tp, "%d", type);
  	strcpy(key, tp); strcat(key, lexema);
	//printf("--------%10s %5d-----------\n", lexema, type);

	comp_dict_item_t* target = NULL;

	target = dict_get2(stack->current->value, key);



	return target;
}
