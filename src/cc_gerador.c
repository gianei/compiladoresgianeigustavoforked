#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "cc_gerador.h"
#include "cc_tree.h"
#include "cc_misc.h"
#include "cc_ast.h"

int reg_jmp_id = 0;
char reg_if[100][10];


char* get_registrador_base(comp_tree_t *tree){
	char* base = malloc (5 * sizeof(char));
	if (((ast_nodo_t*)tree->value)->simbolo->escopo == 0)
		sprintf(base, "%s", "rbss");
	else
		sprintf(base, "%s", "rarp");
	return base;
}

void gera_literal(ast_nodo_t* nodo){
	nodo->local = gera_registrador();
	char* cod = malloc (2500 * sizeof(char));
	sprintf(cod, "loadI %d => %s\n", nodo->simbolo->value.i, nodo->local);
	nodo->codigo = cod;
	//printf("%s\n", nodo->codigo);
}

void gera_identificador(comp_tree_t *tree){
	ast_nodo_t* nodo = (ast_nodo_t*)tree->value;
	nodo->local = gera_registrador();

	char* baseReg = get_registrador_base(tree);
	int desloc = nodo->simbolo->desloc;

	char* cod = malloc (2500 * sizeof(char));
	sprintf(cod,
		"loadAI %s,%d => %s\n",
		baseReg,
		desloc,
		nodo->local);

	nodo->codigo = cod;

	//printf("%s", nodo->codigo);
}

int get_nodo_single_size(ast_nodo_t* nodoIdent){
	switch(nodoIdent->simbolo->varType){
    case SIMBOLO_LITERAL_INT:
      return 4;
      break;
    case SIMBOLO_LITERAL_FLOAT:
      return 8;
      break;
    case SIMBOLO_LITERAL_CHAR:
    case SIMBOLO_LITERAL_BOOL:
      return 1;
      break;
		default:
			printf("ERRO nenhum varType\n");
			return 0;
  }
}

int get_deslocamento_vetor_indexado (comp_tree_t* tree){
	ast_nodo_t* nodoRaiz = (ast_nodo_t*)tree->value;
	ast_nodo_t* nodoIdent = (ast_nodo_t*)tree->first->value;
	ast_nodo_t* nodoExpr = (ast_nodo_t*)tree->first->next->value;
	if (nodoRaiz->tipoNodo != AST_VETOR_INDEXADO){
		printf ("O nodo deve ser um AST_VETOR_INDEXADO!\n");
		return -1;
	}

	int base = nodoIdent->simbolo->desloc;
	int singleSize = get_nodo_single_size(nodoIdent);

	return base + singleSize * nodoExpr->simbolo->value.i;

}

void gera_atribuicao_vetor(comp_tree_t *tree){
	ast_nodo_t* nodoAtr = (ast_nodo_t*)tree->value;

	char* baseReg = get_registrador_base(tree->first->first);

	if (((ast_nodo_t*)tree->first->first->next->value)->tipoNodo == AST_LITERAL){
		int desloc = get_deslocamento_vetor_indexado(tree->first);

		char* cod = malloc (2500 * sizeof(char));
		sprintf(cod,
			"storeAI %s => %s,%d\n",
			((ast_nodo_t*)tree->first->next->value)->local,
			baseReg,
			desloc);
		strcat(nodoAtr->codigo, cod);
	} else { //usar retorno da expressao
		ast_nodo_t* nodoIdentVetor = (ast_nodo_t*)tree->first->first->value;
		ast_nodo_t* nodoExpr       = (ast_nodo_t*)tree->first->first->next->value;
		ast_nodo_t* nodoLiteral		 = (ast_nodo_t*)tree->first->next->value;

		char* resultMulti = gera_registrador();
		char* resultMultiPlusDesloc = gera_registrador();


		char* cod = malloc (2500 * sizeof(char));
		sprintf(cod,
			"%smultI %s,%d => %s\naddI %s,%d  => %s\nstoreAO %s => %s,%s\n",
			nodoExpr->codigo,
			nodoExpr->local, get_nodo_single_size(nodoIdentVetor), resultMulti,
			resultMulti, nodoIdentVetor->simbolo->desloc, resultMultiPlusDesloc,
			nodoLiteral->local, resultMultiPlusDesloc, baseReg);

		strcat(nodoAtr->codigo, cod);
		//printf("%s", nodoAtr->codigo);
	}

	//printf("%s", nodoAtr->codigo);
}



void gera_atribuicao(comp_tree_t *tree){
	ast_nodo_t* nodo = (ast_nodo_t*)tree->value;

	//copia codigo do lado direito da atribuicao
	nodo->codigo = strdup(((ast_nodo_t*)tree->first->next->value)->codigo);

	if (((ast_nodo_t*)tree->first->value)->tipoNodo == AST_VETOR_INDEXADO){
		gera_atribuicao_vetor(tree);
		return;
	}

	char* base = get_registrador_base(tree->first);
	char* cod = malloc (2500 * sizeof(char));
	sprintf(cod,
		"storeAI %s => %s,%d\n",
		((ast_nodo_t*)tree->first->next->value)->local,
		base,
		((ast_nodo_t*)tree->first->value)->simbolo->desloc);
	strcat(nodo->codigo, cod);


	//printf("%s", nodo->codigo);

	//tree_debug_print(tree->first->next);
}


void gera_op_aritm(comp_tree_t *tree, char *op){
	ast_nodo_t* nodoRaiz = (ast_nodo_t*)tree->value;
	ast_nodo_t* nodoEsq = (ast_nodo_t*)tree->first->value;
	ast_nodo_t* nodoDir = (ast_nodo_t*)tree->first->next->value;

	nodoRaiz->local = gera_registrador();

	char* cod = malloc (2500 * sizeof(char));
	sprintf(cod, "%s%s%s %s,%s => %s\n",
		nodoEsq->codigo,
		nodoDir->codigo,
		op,
	 	nodoEsq->local,
	  nodoDir->local,
		nodoRaiz->local);

	nodoRaiz->codigo = cod;
	//printf("%s",nodoRaiz->codigo);
}

void gera_op_comp(comp_tree_t *tree, char *op){
	ast_nodo_t* nodoRaiz = (ast_nodo_t*)tree->value;
	ast_nodo_t* nodoEsq = (ast_nodo_t*)tree->first->value;
	ast_nodo_t* nodoDir = (ast_nodo_t*)tree->first->next->value;

	nodoRaiz->local = gera_registrador();

	char* cod = malloc (2500 * sizeof(char));
	sprintf(cod, "%s%s%s %s,%s -> %s\n",
		nodoEsq->codigo,
		nodoDir->codigo,
		op,
	 	nodoEsq->local,
	  nodoDir->local,
		nodoRaiz->local);

	nodoRaiz->codigo = cod;
	strcpy(reg_if[reg_jmp_id], nodoRaiz->local);
	reg_jmp_id++;
}

char* gera_rotulo(){
    char* key = malloc (5+sizeof(int)+3);
    strcpy(key, "l");
    char tp[10]; sprintf(tp, "%d", rotulo_id++);
    strcat(key, tp);

    return key;
}

void gera_if(comp_tree_t *tree){
	ast_nodo_t* nodoIf = (ast_nodo_t*)tree->value;
	ast_nodo_t* nodoBlocoExpr = (ast_nodo_t*)tree->first->value;
	ast_nodo_t* nodoBlocoUm = (ast_nodo_t*)tree->first->next->value;
	ast_nodo_t* nodoBlocoDois = NULL;
	if (tree->first->next->next != NULL)
	 	nodoBlocoDois = (ast_nodo_t*)tree->first->next->next->value;
	ast_nodo_t* nodoBlocoLast = (ast_nodo_t*)tree->last->value;
	if (nodoBlocoDois == NULL)
		nodoBlocoDois = nodoBlocoLast;
	char* cod = malloc (2500 * sizeof(char));
	char* rotulo1;
	char* rotulo2;
	char* rotuloLast;

	rotulo1 = gera_rotulo();
	rotulo2 = gera_rotulo();
	rotuloLast = gera_rotulo();

	strcat(cod, nodoBlocoExpr->codigo);
	strcat(cod, "cbr ");reg_jmp_id--;
	strcat(cod, reg_if[reg_jmp_id]);
	strcat(cod, " -> ");
	strcat(cod, rotulo1);
	strcat(cod, ",");
	strcat(cod, rotulo2);
	strcat(cod, "\n");


	strcat(cod, rotulo1);
	strcat(cod, ": ");
	strcat(cod, nodoBlocoUm->codigo);
	if (nodoIf->isIfWithElse){
		strcat(cod, "jumpI -> ");
		strcat(cod, rotuloLast);
		strcat(cod, "\n");
	}

	if (nodoBlocoLast != nodoBlocoUm){
		strcat(cod, rotulo2);
		strcat(cod, ": ");
		strcat(cod, nodoBlocoDois->codigo);
	}	else {
		strcat(cod, rotulo2);
		strcat(cod, ": nop\n");
	}

	if (nodoBlocoLast == nodoBlocoDois)
	{
		if (nodoIf->isIfWithElse){
			strcat(cod, rotuloLast);
			strcat(cod, ": nop\n");
		}
	} else { //se são diferentes, obrigatoriamente há um else
		strcat(cod, rotuloLast);
		strcat(cod, ": ");
		strcat(cod, nodoBlocoLast->codigo);
	}
	nodoIf->codigo = cod;
}

void gera_do_while(comp_tree_t *tree){
	ast_nodo_t* nodoWhile = (ast_nodo_t*)tree->value;
	ast_nodo_t* nodoBloco = (ast_nodo_t*)tree->first->value;
	ast_nodo_t* nodoExpr  = (ast_nodo_t*)tree->first->next->value;

	char* cod = malloc (2500 * sizeof(char));
	char* rotuloBloco = gera_rotulo();
	char* rotuloNext = gera_rotulo();

	strcat(cod, rotuloBloco);
	strcat(cod, ": ");
	strcat(cod, nodoBloco->codigo);

	strcat(cod, nodoExpr->codigo);
	strcat(cod, "cbr ");reg_jmp_id--;
	strcat(cod, reg_if[reg_jmp_id]);
	strcat(cod, " -> ");
	strcat(cod, rotuloBloco);
	strcat(cod, ",");
	strcat(cod, rotuloNext);
	strcat(cod, "\n");

	strcat(cod, rotuloNext);
	strcat(cod, ": nop\n");

	nodoWhile->codigo = cod;
}

void gera_while_do(comp_tree_t *tree){
	ast_nodo_t* nodoWhile = (ast_nodo_t*)tree->value;
	ast_nodo_t* nodoBloco = (ast_nodo_t*)tree->first->value;
	ast_nodo_t* nodoExpr  = (ast_nodo_t*)tree->first->next->value;

	char* cod = malloc (2500 * sizeof(char));
	char* rotuloExpr = gera_rotulo();
	char* rotuloBloco = gera_rotulo();
	char* rotuloNext = gera_rotulo();

	strcat(cod, rotuloExpr);
	strcat(cod, ": ");
	strcat(cod, nodoExpr->codigo);
	strcat(cod, "cbr ");reg_jmp_id--;
	strcat(cod, reg_if[reg_jmp_id]);
	strcat(cod, " -> ");
	strcat(cod, rotuloBloco);
	strcat(cod, ",");
	strcat(cod, rotuloNext);
	strcat(cod, "\n");

	strcat(cod, rotuloBloco);
	strcat(cod, ": ");
	strcat(cod, nodoBloco->codigo);

	strcat(cod, "jumpI -> ");
	strcat(cod, rotuloExpr);
	strcat(cod, "\n");

	strcat(cod, rotuloNext);
	strcat(cod, ": nop\n");

	nodoWhile->codigo = cod;
}

void tree_gera_node(comp_tree_t *tree){
	if (tree == NULL) return;
	ast_nodo_t* meuNodo = (ast_nodo_t*)tree->value;
	if (meuNodo == NULL)
		printf("Nodo nulo");
	else{
		switch (meuNodo->tipoNodo) {
			case AST_ATRIBUICAO:
				gera_atribuicao(tree);
				if (tree->childnodes > 2){
					strcat(((ast_nodo_t*)tree->value)->codigo, ((ast_nodo_t*)tree->last->value)->codigo);
				}
				break;
			case AST_IDENTIFICADOR:
				gera_identificador(tree);
				break;
			case AST_LITERAL:
				gera_literal(meuNodo);
				break;
			case AST_ARIM_SOMA:
				gera_op_aritm(tree, "add");
				break;
			case AST_ARIM_SUBTRACAO:
				gera_op_aritm(tree, "sub");
				break;
			case AST_ARIM_MULTIPLICACAO:
				gera_op_aritm(tree, "mult");
				break;
			case AST_ARIM_DIVISAO:
				gera_op_aritm(tree, "div");
				break;
			case AST_LOGICO_E:
				gera_op_comp(tree, "and");
				break;
			case AST_LOGICO_OU:
				gera_op_comp(tree, "or");
				break;
			case AST_LOGICO_COMP_IGUAL:
				gera_op_comp(tree, "cmp_EQ");
				break;
			case AST_LOGICO_COMP_DIF:
				gera_op_comp(tree, "cmp_NE");
				break;
			case AST_LOGICO_COMP_G:
				gera_op_comp(tree, "cmp_GT");
				break;
			case AST_LOGICO_COMP_L:
				gera_op_comp(tree, "cmp_LT");
				break;
			case AST_LOGICO_COMP_GE:
				gera_op_comp(tree, "cmp_GE");
				break;
			case AST_LOGICO_COMP_LE:
				gera_op_comp(tree, "cmp_LE");
				break;
			case AST_IF_ELSE:
				gera_if(tree);
				break;
			case AST_DO_WHILE:
				gera_do_while(tree);
				break;
			case AST_WHILE_DO:
				gera_while_do(tree);
				break;
			case AST_PROGRAMA:
				printf("%s", ((ast_nodo_t*)tree->first->value)->codigo);
			default:
				meuNodo->codigo = ((ast_nodo_t*)tree->first->value)->codigo;
		}

/*
	if(((simbolo_t*)((ast_nodo_t*)tree->value)->simbolo) != NULL)
	printf("%s", ((simbolo_t*)((ast_nodo_t*)tree->value)->simbolo)->lexema);
	else
	printf("tipo: %d", ((ast_nodo_t*)tree->value)->tipoNodo);
	getchar();
	if(((ast_nodo_t*)tree->value)->tipoNodo == 17)
	printf("-> %d\n", ((ast_nodo_t*)tree->first->value)->tipoNodo);*/

		//if (meuNodo->simbolo != NULL){		}


	}

}

void run_tree(comp_tree_t *tree){
	if (tree == NULL) return ;

	comp_tree_t *ptr = tree;
	do {

		if (ptr->first != NULL)
			run_tree(ptr->first);

		tree_gera_node(ptr);

		ptr = ptr->next;
	} while(ptr != NULL);
}

void gera_compilado(comp_tree_t* tree){
  run_tree(tree);
}
