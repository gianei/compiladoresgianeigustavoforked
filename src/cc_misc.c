#include <main.h>
#include <stdlib.h>
#include <string.h>
#include "cc_misc.h"
#include "cc_stack.h"
#include "cc_gerador.h"

comp_dict_t* dick;
comp_tree_t* tree;

stack_t *parametersStack;

stack_t* stack;

int parserReturn = SINTATICA_SUCESSO;
int global_id;
int rotulo_id;
int fp;
int rbss;

//tipo da funcao atual
int func_type;

void* tableInsert(char *key, int type, int line){
  // cria a key do dicionario junto com o tipo (especificação)
  char* newKey = malloc(strlen(key)+2);
  switch (type) {
    case SIMBOLO_LITERAL_INT:
      newKey[0] = '0';
      break;
    case SIMBOLO_LITERAL_FLOAT:
      newKey[0] = '1';
      break;
    case SIMBOLO_LITERAL_CHAR:
      newKey[0] = '2';
      break;
    case SIMBOLO_LITERAL_STRING:
      newKey[0] = '4';
      break;
    case SIMBOLO_LITERAL_BOOL:
      newKey[0] = '5';
      break;
    case SIMBOLO_IDENTIFICADOR:
      newKey[0] = '6';
      break;
    default:
      newKey[0] = ' ';
      break;
  }

  memcpy(&newKey[1], key, strlen(key));
  newKey[strlen(key)+1] = '\0'; // strlen(key) + 2 - 1 (index 0)

  simbolo_t *simb = dict_get(dick, newKey);

  if (simb){
    simb->line = line;
    simb->type = type;

    free(newKey);
    return simb;
  } else {
    simb = malloc(sizeof(simbolo_t));
    simb->line = line;
    simb->lexema = strdup(key);
    simb->type = type;
    simb->mod = 0;
    simb->userType = NULL;
    simb->parameters = NULL;

    char *str;
    switch (type) {
      case SIMBOLO_LITERAL_INT:
        simb->value.i = atoi(key);
        break;
      case SIMBOLO_LITERAL_FLOAT:
        simb->value.f = atof(key);
        break;
      case SIMBOLO_LITERAL_CHAR:
        simb->value.c = key[1];
        break;
      case SIMBOLO_LITERAL_STRING:
        str = malloc(strlen(key) - 2);
        memcpy(str, &key[1], strlen(key) -2);
        simb->value.s = str;
        break;
      case SIMBOLO_LITERAL_BOOL:
        simb->value.b = (strcmp(key, "true") ? 1 : 0);
        break;
      case SIMBOLO_IDENTIFICADOR:
        simb->value.s = strdup(key);
        break;
    }

    void *retorno = dict_put(dick, newKey, simb);

    free(newKey);

    return retorno;

  }
}

int comp_get_line_number (void)
{
  //implemente esta função
  return myLineNumber;
}

void yyerror (char const *mensagem)
{
  parserReturn = SINTATICA_ERRO;
  fprintf (stderr, "%s, line: %d\n", mensagem, myLineNumber); //altere para que apareça a linha
}

void main_init (int argc, char **argv)
{
  dick = dict_new();
  //dickstree = tree_new();
  tree = tree_new();
  //dickstree_atual = dickstree;

  stack = stack_new();

  global_id=1;
  rotulo_id=0;
  fp=0;
  rbss=0;
  //implemente esta função com rotinas de inicialização, se necessário
}

void main_finalize (void)
{
  //for testing

  //tree_debug_print(tree);

  //dict_debug_print(dick);

  gera_compilado(tree);
  //comp_print_table();

  //stack_debug_print(stack);
  exit(IKS_SUCCESS);
}

void comp_print_table_aux(comp_dict_item_t * item)
{
  while (item != NULL) {
    simbolo_t* valor = (simbolo_t*) item->value;
    cc_dict_etapa_2_print_entrada(valor->lexema, valor->line, valor->type);

    // for testing
    /*if (valor->userType){
        printf("userType = %s\n", valor->userType->lexema);
    }
    printf("varType = %d\n", valor->varType);
    printf("lexema = %s\n", valor->lexema);*/

    item = item->next;
  }
}

void comp_print_table(void)
{
  int i, l;
  for (i = 0, l = dick->size; i < l; ++i) {
    if (dick->data[i]) {
      comp_print_table_aux(dick->data[i]);
    }
  }
}

static char* sort_type(int type)
{
  char* name = malloc(sizeof(char)*7);
  switch(type){
    case SIMBOLO_LITERAL_INT:
      strcpy(name, "int");
      break;
    case SIMBOLO_LITERAL_FLOAT:
      strcpy(name, "float");
      break;
    case SIMBOLO_LITERAL_CHAR:
      strcpy(name, "char");
      break;
    case SIMBOLO_LITERAL_STRING:
      strcpy(name, "string");
      break;
    case SIMBOLO_LITERAL_BOOL:
      strcpy(name, "bool");
      break;
  }
  //strcat(name, "\0");

  return name;

}

static void change_mod(int type_atr, simbolo_t* target)
{
  switch(type_atr){
    case SIMBOLO_LITERAL_INT:
      target->mod=1;
      break;
    case SIMBOLO_LITERAL_FLOAT:
      target->mod=2;
      break;
    case SIMBOLO_LITERAL_BOOL:
      target->mod=3;
      break;
  }
  //se for NULL eh porque a expressao nao contem identificadores
  //if(target->lexema!=NULL)
  	//printf("\nmudar %s para %s\n", target->lexema, sort_type(target->mod));
}

void check_type(char* lexema, int type_decl, int type_atr, simbolo_t* target, int id)
{
   int error=0;
   if(type_atr==6)
	type_atr = ((simbolo_t*) search_stack(stack, target->lexema, 6)->value)->varType;

   switch(type_decl){
    case SIMBOLO_LITERAL_INT:
      if(type_decl!=type_atr && type_atr!=SIMBOLO_LITERAL_FLOAT && type_atr!=SIMBOLO_LITERAL_BOOL)
	error=1;
      else if(type_decl!=type_atr)
	change_mod(type_decl, target);
      break;
    case SIMBOLO_LITERAL_FLOAT:
      if(type_decl!=type_atr && type_atr!=SIMBOLO_LITERAL_INT && type_atr!=SIMBOLO_LITERAL_BOOL)
	error=1;
      else if(type_decl!=type_atr)
	change_mod(type_decl, target);
      //else
	//if(type_atri==SIMBOLO_LITERAL_INT)
	   //;
      break;
    case SIMBOLO_LITERAL_CHAR:
      if(type_decl!=type_atr)
	error=1;
      break;
    case SIMBOLO_LITERAL_STRING:
      if(type_decl!=type_atr)
	error=1;
      break;
    case SIMBOLO_LITERAL_BOOL:
      if(type_decl!=type_atr && type_atr!=SIMBOLO_LITERAL_INT && type_atr!=SIMBOLO_LITERAL_FLOAT)
	error=1;
      else if(type_decl!=type_atr)
	change_mod(type_decl, target);
      break;
  }

  if(error)
  {
    if(id == 1)
	    printf("\nElement of vector ");
      else if (id == 2)
        printf("\n Argument ");
          else
	          printf("\nVariable ");

      printf("\"%s\" of type %s, given %s, line %d\n", lexema, sort_type(type_decl), sort_type(type_atr), myLineNumber);

      if (id == 2)
        exit(IKS_ERROR_WRONG_TYPE_ARGS);

      if(type_atr==SIMBOLO_LITERAL_CHAR)
	exit(IKS_ERROR_CHAR_TO_X);
      else
      {
      	if(type_atr==SIMBOLO_LITERAL_STRING)
		exit(IKS_ERROR_STRING_TO_X);
	else
      		exit(IKS_ERROR_WRONG_TYPE);
      }
  }
}

char* gera_registrador()
{
    char tp[10]; sprintf(tp, "%d", global_id++);
    char* key = malloc (5+sizeof(int)+1);
	    strcpy(key, "r"); strcat(key, tp);
	return key;
}

int size_tipo(int tipo)
{
    int size;

    switch(tipo)
    {
	    case SIMBOLO_LITERAL_INT:
	      size = 4;
	      break;
	    case SIMBOLO_LITERAL_FLOAT:
	      size = 8;
	      break;
	    case SIMBOLO_LITERAL_CHAR:
	      size = 1;
	      break;
	    case SIMBOLO_LITERAL_BOOL:
	      size = 1;
	      break;
    }

    return size;
}

void parse_aritmetic(ast_nodo_t* n1, ast_nodo_t* n2, ast_nodo_t* n3){

  //max int tem 10 casas
  char* key = gera_registrador();

  n2->simbolo = malloc(sizeof(simbolo_t));
  n2->simbolo->lexema = key;

  int type_test1 = n1->simbolo->type;
  int type_test3 = n3->simbolo->type;

  if(type_test1 == 6)
  	type_test1 = ((simbolo_t*) search_stack(stack, n1->simbolo->lexema, 6)->value)->varType;

  if(type_test3 == 6)
  	type_test3 = ((simbolo_t*) search_stack(stack, n3->simbolo->lexema, 6)->value)->varType;

  if(type_test1==SIMBOLO_LITERAL_CHAR || type_test3==SIMBOLO_LITERAL_CHAR){
    printf("\nCan not use char, line %d\n", myLineNumber);
	  exit(IKS_ERROR_CHAR_TO_X);
  }

  if(type_test1==SIMBOLO_LITERAL_STRING || type_test3==SIMBOLO_LITERAL_STRING){
    printf("\nCan not use string, line %d\n", myLineNumber);
	  exit(IKS_ERROR_STRING_TO_X);
  }

  if(type_test1==SIMBOLO_LITERAL_FLOAT || type_test3==SIMBOLO_LITERAL_FLOAT)
  {
	n2->simbolo->type = SIMBOLO_LITERAL_FLOAT;
	//forca ambos a serem float
	if(type_test1!=SIMBOLO_LITERAL_FLOAT)
		n1->simbolo->mod = 2;
	if(type_test3!=SIMBOLO_LITERAL_FLOAT)
	{
		comp_dict_item_t* target = search_stack(stack, n3->simbolo->lexema, 0);
		if(target!=NULL)
			((simbolo_t*)target->value)->mod = 2;
		else
			n3->simbolo->mod=2;
	}

	((simbolo_t*)tableInsert(key, SIMBOLO_LITERAL_FLOAT, myLineNumber))->varType = SIMBOLO_LITERAL_FLOAT;
  }
  else
  {
  	n2->simbolo->type = SIMBOLO_LITERAL_INT;
	((simbolo_t*)tableInsert(key, SIMBOLO_LITERAL_INT, myLineNumber))->varType = SIMBOLO_LITERAL_INT;
  }
  global_id++;

//printf("----------%4s %s -> %d------------\n", n1->simbolo->lexema, n3->simbolo->lexema, n3->simbolo->type!=SIMBOLO_LITERAL_FLOAT);



}
