/*
  Grupo 09

  Gianei Leandro Sebastiany
  Gustavo Laguna de Souza
*/
%{
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "main.h"
#include "cc_ast.h"
#include "cc_misc.h"
#include "cc_stack.h"

%}

%union {
  int myType;

  // deveria ser do tipo simbolo_t* ao invés de void*
  void* valor_simbolo_lexico;
  comp_tree_t* nodoArvore;
}
/* Declaração dos tokens da linguagem */
%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_PR_CONST
%token TK_PR_STATIC
%token TK_PR_FOREACH
%token TK_PR_FOR
%token TK_PR_SWITCH
%token TK_PR_CASE
%token TK_PR_BREAK
%token TK_PR_CONTINUE
%token TK_PR_CLASS
%token TK_PR_PRIVATE
%token TK_PR_PUBLIC
%token TK_PR_PROTECTED
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_OC_SL
%token TK_OC_SR
%token TK_LIT_INT
%token TK_LIT_FLOAT
%token TK_LIT_FALSE
%token TK_LIT_TRUE
%token TK_LIT_CHAR
%token TK_LIT_STRING
%token TK_IDENTIFICADOR
%token TOKEN_ERRO

%type<myType> TIPO

%type<myType> DECL_TIPO_STATIC
%type<myType> DECL_TIPO_CONST
%type<myType> DECL_TIPO_CONST_STATIC
%type<valor_simbolo_lexico> DECL_TIPO_STATIC_USER
%type<valor_simbolo_lexico> DECL_TIPO_CONST_USER
//%type<valor_simbolo_lexico> DECL_TIPO_CONST_STATIC_USER
%type<valor_simbolo_lexico> DECL_VAR_GLOBAL
%type<valor_simbolo_lexico> DECL_TIPO_USUARIO
%type<valor_simbolo_lexico> TK_IDENTIFICADOR
%type<valor_simbolo_lexico> FUNCAO_CABECALHO
%type<valor_simbolo_lexico> TK_LIT_INT
%type<valor_simbolo_lexico> TK_LIT_FLOAT
%type<valor_simbolo_lexico> TK_LIT_CHAR
%type<valor_simbolo_lexico> TK_LIT_STRING
%type<valor_simbolo_lexico> TK_LIT_TRUE
%type<valor_simbolo_lexico> TK_LIT_FALSE
%type<valor_simbolo_lexico> TK_OC_LE
%type<valor_simbolo_lexico> TK_PR_RETURN
%type<nodoArvore> programaLocal
%type<nodoArvore> programaFluxo
%type<nodoArvore> comandoSimples
%type<nodoArvore> comandoLocal
%type<nodoArvore> DECL_VAR_LOCAL
%type<nodoArvore> VETOR_INDEXADO
%type<nodoArvore> ATRIBUICAO
%type<nodoArvore> LITERAL
%type<nodoArvore> BLOCO_DE_COMANDOS
%type<nodoArvore> OPERADOR_ARITMETICO_1
%type<nodoArvore> OPERADOR_ARITMETICO_2
%type<nodoArvore> OP_ARITMETICA
%type<nodoArvore> E
%type<nodoArvore> T
%type<nodoArvore> F
%type<nodoArvore> FATOR
%type<nodoArvore> programacao
%type<nodoArvore> comandoGlobal
%type<nodoArvore> DECL_FUNCAO
%type<nodoArvore> EXPRESSAO
%type<nodoArvore> EXPRESSAO_ARITMETICA
%type<nodoArvore> EXPRESSAO_LOGICA
%type<nodoArvore> OPERADOR_RELACIONAL
%type<nodoArvore> OPERADOR_DIFERENCA
%type<nodoArvore> OPERADOR_IGUALDADE
%type<nodoArvore> OPERADOR_LOGICO
%type<nodoArvore> INPUT
%type<nodoArvore> OUTPUT
%type<nodoArvore> OUTPUTS
//%type<nodoArvore> BOOL
//%type<nodoArvore> TERMO
%type<nodoArvore> OP_LOGICA
%type<nodoArvore> OP_LOGICA_ENCADEADAS
%type<nodoArvore> TERMINADOR
%type<nodoArvore> CHAMADA_DE_FUNCAO
%type<nodoArvore> LISTA_EXPRESSAO
%type<nodoArvore> CONTROLE
%type<nodoArvore> CONTROLE_DE_FLUXO
%type<myType> LISTA_DE_DIMENSOES


//ainda não foi necessário usar
//%right "*" "/"
//%precedence '*'
//%precedence '/'
//%right TK_PR_THEN TK_PR_ELSE
//%nonassoc TK_PR_WHILE TK_PR_DO

%%
/* Regras (e ações) da gramática */
programa:
{
  stack_push(stack, stack_make_node(dick));
}
programacao {
  //ajusta valor do nodo inicial programa
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_PROGRAMA;
  tree->value = node;

  //insere o primeiro filho
  if ($2 != NULL)
    tree_insert_node(tree, $2);

};
programacao: comandoGlobal {
  if ($1 != NULL)
    $$ = $1;
};
programacao: comandoGlobal programacao {
  if ($1 == NULL && $2 != NULL) {
    $$ = $2;
  } else {
    if ($2 != NULL)
      tree_insert_node($1, $2);
  }
};

comandoGlobal: DECL_VAR_GLOBAL {$$ = NULL;};
comandoGlobal: DECL_TIPO_USUARIO {$$ = NULL;};
comandoGlobal: DECL_FUNCAO
{
  $$ = $1;
  stack_pop(stack);
  dick = stack->current->value;
};
comandoGlobal: DECL_VETOR_GLOBAL {$$ = NULL;};



/* Bloco de comandos */
BLOCO_DE_COMANDOS: '{' '}'
{
  $$ = NULL;
};
BLOCO_DE_COMANDOS: '{' programaLocal '}'
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_BLOCO;
  if ($2 == NULL)
    $$ = tree_make_node(node);
  else
    $$ = tree_make_unary_node(node, $2);


};

programaLocal: comandoSimples { $$ = $1; };
programaLocal: comandoSimples programaLocal
{
  if ($1 != NULL){
    if ($2 != NULL)
      tree_insert_node($1, $2);
    $$ = $1;
  } else {
    $$ = $2;
  }
};

comandoSimples: TERMINADOR { $$ = NULL; };
comandoSimples: comandoLocal TERMINADOR { $$ = $1; };
comandoSimples: CONTROLE_DE_FLUXO TERMINADOR { $$ = $1; };
comandoSimples: CASE ':' { $$ = NULL; };
comandoSimples: '{'
{
  dick = dict_new();
  stack_push(stack, stack_make_node(dick));
}
 programaLocal '}' TERMINADOR
{
  stack_pop(stack);
  dick = stack->current->value;

  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_BLOCO;
  if ($3 == NULL)
    $$ = tree_make_node(node);
  else
    $$ = tree_make_unary_node(node, $3);
};
comandoSimples: '{' '}' TERMINADOR { $$ = NULL; };


comandoLocal: DECL_VAR_LOCAL { if ($1 != NULL) {$$ = $1;} else {$$ = NULL;}};
comandoLocal: ATRIBUICAO { $$ = $1; };
comandoLocal: INPUT { $$ = $1; };
comandoLocal: OUTPUTS { $$ = $1; };
comandoLocal: CHAMADA_DE_FUNCAO { $$ = $1; };
comandoLocal: SHIFT { $$ = NULL; };
comandoLocal: CONTROLE { $$ = $1;};

comandoLocalFor: DECL_VAR_LOCAL | ATRIBUICAO | INPUT | OUTPUT  | CHAMADA_DE_FUNCAO | SHIFT | CONTROLE;

programaFluxo: comandoLocal TERMINADOR { $$ = $1; };
programaFluxo: '{'

{
  dick = dict_new();
  stack_push(stack, stack_make_node(dick));
}
 programaLocal '}' TERMINADOR
{
  stack_pop(stack);
  dick = stack->current->value;

  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_BLOCO;
  if ($3 == NULL)
    $$ = tree_make_node(node);
  else
    $$ = tree_make_unary_node(node, $3);
};






TIPO:
      TK_PR_INT {$$ = SIMBOLO_LITERAL_INT;}
    | TK_PR_FLOAT {$$ = SIMBOLO_LITERAL_FLOAT;}
    | TK_PR_BOOL {$$ = SIMBOLO_LITERAL_CHAR;}
    | TK_PR_CHAR {$$ = SIMBOLO_LITERAL_CHAR;}
    | TK_PR_STRING {$$ = SIMBOLO_LITERAL_STRING;}
;

DECL_TIPO_STATIC:
      TIPO {$$ = $1;}
    | TK_PR_STATIC TIPO {$$ = $2;}
;
DECL_TIPO_STATIC_USER:
      TK_IDENTIFICADOR {$$ = $1;}
    | TK_PR_STATIC TK_IDENTIFICADOR {$$ = $2;}
;
DECL_TIPO_CONST:
      TIPO {$$ = $1;}
    | TK_PR_CONST TIPO {$$ = $2;}
;
DECL_TIPO_CONST_USER:
      TK_IDENTIFICADOR {$$ = $1;}
    | TK_PR_CONST TK_IDENTIFICADOR {$$ = $2;}
;
DECL_TIPO_CONST_STATIC:
  TIPO {$$ = $1;}
  | TK_PR_CONST TIPO {$$ = $2;}
  | TK_PR_STATIC TK_PR_CONST TIPO {$$ = $3;}
;
/*DECL_TIPO_CONST_STATIC_USER:
  TK_IDENTIFICADOR {$$ = $1;}
  | TK_PR_CONST TK_IDENTIFICADOR {$$ = $2;}
  | TK_PR_STATIC TK_PR_CONST TK_IDENTIFICADOR {$$ = $3;}
;*/

ENCAPSULAMENTO: TK_PR_PROTECTED | TK_PR_PRIVATE | TK_PR_PUBLIC;

TERMINADOR: ';' { ; };

LITERAL: TK_LIT_INT
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  node->simbolo->varType = SIMBOLO_LITERAL_INT;
  $$ = tree_make_node(node);
};
LITERAL: TK_LIT_FLOAT
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  node->simbolo->varType = SIMBOLO_LITERAL_FLOAT;
  $$ = tree_make_node(node);
};
LITERAL: '-' TK_LIT_INT
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $2;
  node->simbolo->varType = SIMBOLO_LITERAL_INT;
  $$ = tree_make_node(node);
};
LITERAL: '-' TK_LIT_FLOAT
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $2;
  node->simbolo->varType = SIMBOLO_LITERAL_FLOAT;
  $$ = tree_make_node(node);
};
LITERAL: TK_LIT_FALSE
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  node->simbolo->varType = SIMBOLO_LITERAL_BOOL;
  $$ = tree_make_node(node);
};
LITERAL: TK_LIT_TRUE
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  node->simbolo->varType = SIMBOLO_LITERAL_BOOL;
  $$ = tree_make_node(node);
};
LITERAL: TK_LIT_CHAR
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  node->simbolo->varType = SIMBOLO_LITERAL_CHAR;
  $$ = tree_make_node(node);
};
LITERAL: TK_LIT_STRING
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  node->simbolo->varType = SIMBOLO_LITERAL_STRING;
  $$ = tree_make_node(node);
};

/* Declaração de novo tipo */
DECL_TIPO_USUARIO: TK_PR_CLASS TK_IDENTIFICADOR LISTA_DE_CAMPOS TERMINADOR
{
  ((simbolo_t*)$2)->isUserType = 1;
  $$ = ((simbolo_t*)$2);
};
LISTA_DE_CAMPOS: '[' ']';
LISTA_DE_CAMPOS: '[' CAMPOS ']';
CAMPOS: CAMPO;
CAMPOS: CAMPO CAMPO_F;
CAMPO_F: ':' CAMPO;
CAMPO_F: ':' CAMPO CAMPO_F;
CAMPO: ENCAPSULAMENTO DECL_TIPO_STATIC TK_IDENTIFICADOR;

/* Declaração de variáveis globais */
DECL_VAR_GLOBAL: DECL_TIPO_STATIC TK_IDENTIFICADOR TERMINADOR {
  $$ = $2;

  if (((simbolo_t*)$2)->varType > 0){
    printf("\nVariable %s declared, line %d\n", ((simbolo_t*)$2)->lexema, myLineNumber);
    exit(IKS_ERROR_DECLARED);
  }
  ((simbolo_t*)$2)->varType = $1;
  ((simbolo_t*)$2)->dictType = DICT_TYPE_VAR;

  switch($1){
    case SIMBOLO_LITERAL_INT:
      ((simbolo_t*)$2)->size = 4;
      break;
    case SIMBOLO_LITERAL_FLOAT:
      ((simbolo_t*)$2)->size = 8;
      break;
    case SIMBOLO_LITERAL_CHAR:
      ((simbolo_t*)$2)->size = 1;
      break;
    case SIMBOLO_LITERAL_STRING:
      ((simbolo_t*)$2)->size = sizeof(((simbolo_t*)$2)->value.s);
      break;
    case SIMBOLO_LITERAL_BOOL:
      ((simbolo_t*)$2)->size = 1;
      break;
  }

  ((simbolo_t*)$2)->escopo = 0;
  ((simbolo_t*)$2)->desloc = rbss;

  rbss += ((simbolo_t*)$2)->size;
};
/* Declaração de variável global com tipo do usuário*/
DECL_VAR_GLOBAL: DECL_TIPO_STATIC_USER TK_IDENTIFICADOR TERMINADOR
{
  //if (((simbolo_t*)$1)->isUserType) {
    ((simbolo_t*)$2)->userType = $1;
    $$ = $2;
    ((simbolo_t*)$2)->dictType = DICT_TYPE_VAR;
  //} else {
  //  yyerror("ERRO: Tipo de usuario inexistente");
  //}
};

/* Declaração de vetores globais */
DECL_VETOR_GLOBAL: DECL_TIPO_STATIC TK_IDENTIFICADOR '[' LISTA_DE_DIMENSOES ']' TERMINADOR {
  ((simbolo_t*)$2)->varType = $1;
  ((simbolo_t*)$2)->dictType = DICT_TYPE_VEC;

  switch($1){
    case SIMBOLO_LITERAL_INT:
      ((simbolo_t*)$2)->size = 4 * $4;
      break;
    case SIMBOLO_LITERAL_FLOAT:
      ((simbolo_t*)$2)->size = 8 * $4;
      break;
    case SIMBOLO_LITERAL_CHAR:
      ((simbolo_t*)$2)->size = 1 * $4;
      break;
    case SIMBOLO_LITERAL_STRING:
      ((simbolo_t*)$2)->size = sizeof(((simbolo_t*)$2)->value.s) * $4;
      break;
    case SIMBOLO_LITERAL_BOOL:
      ((simbolo_t*)$2)->size = 1 * $4;
      break;
  }

  ((simbolo_t*)$2)->escopo = 0;
  ((simbolo_t*)$2)->desloc = rbss;

  rbss += ((simbolo_t*)$2)->size;
};

LISTA_DE_DIMENSOES: TK_LIT_INT
{
  $$ = ((simbolo_t*)$1)->value.i;
};
LISTA_DE_DIMENSOES: TK_LIT_INT ',' LISTA_DE_DIMENSOES
{
  $$ = ((simbolo_t*)$1)->value.i * $3;
};

/* Declaração de funções */
DECL_FUNCAO: FUNCAO_CABECALHO BLOCO_DE_COMANDOS
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_FUNCAO;
  node->simbolo = $1;
  if($2 != NULL)
    $$ = tree_make_unary_node(node, $2);
  else
    $$ = tree_make_node(node);

  fp = 1024; //pq os testes tem esse desloc
};
FUNCAO_CABECALHO: DECL_TIPO_STATIC TK_IDENTIFICADOR {
  parametersStack = stack_new();
  func_type = $1;

  if(search_stack_local(stack, ((simbolo_t*)$2)->lexema, 6)!=NULL){
    if (((simbolo_t*)$2)->varType > 0){
      printf("\nVariable %s declared, line %d\n", ((simbolo_t*)$2)->lexema, myLineNumber);
      exit(IKS_ERROR_DECLARED);
    }
  }

  dick = dict_new();
  stack_push(stack, stack_make_node(dick));
}
FUNCAO_LISTA
{
  ((simbolo_t*)$2)->parameters = parametersStack;
  $$ = $2;
  //diferencia se uma função foi declarada
  ((simbolo_t*)$$)->varType = $1;
  ((simbolo_t*)$$)->dictType = DICT_TYPE_FUNC;
};
//habilitar se função pode ser de tipo de usuário
FUNCAO_CABECALHO: DECL_TIPO_STATIC_USER TK_IDENTIFICADOR
{
  parametersStack = stack_new();

  if(search_stack_local(stack, ((simbolo_t*)$2)->lexema, 6)!=NULL){
    if (((simbolo_t*)$2)->varType > 0){
      printf("\nVariable %s declared, line %d\n", ((simbolo_t*)$2)->lexema, myLineNumber);
      exit(IKS_ERROR_DECLARED);
    }
  }

  dick = dict_new();
  stack_push(stack, stack_make_node(dick));
}
FUNCAO_LISTA
{
  $$ = $2;
  //if (((simbolo_t*)$1)->isUserType) {
    ((simbolo_t*)$2)->userType = $1;
    $$ = $2;
    ((simbolo_t*)$$)->dictType = DICT_TYPE_FUNC;
  //} else {
  //  yyerror("ERRO: Tipo de usuario inexistente");
  //}
};
FUNCAO_LISTA: '(' ')';
FUNCAO_LISTA: '(' PARAMETROS ')';
PARAMETROS: PARAMETRO;
PARAMETROS: PARAMETRO PARAMETRO_F;
PARAMETRO_F: ',' PARAMETRO;
PARAMETRO_F: ',' PARAMETRO PARAMETRO_F;
PARAMETRO: DECL_TIPO_CONST TK_IDENTIFICADOR
{
  ((simbolo_t*)$2)->varType = $1;
  int *myInt = malloc(sizeof(int));
  *myInt = $1;
  stack_push(parametersStack, stack_make_node(myInt));
};
PARAMETRO: DECL_TIPO_CONST_USER TK_IDENTIFICADOR
{
  //stack_push(parametersStack, stack_make_node(&$1));
  //if (((simbolo_t*)$1)->isUserType) {
    ((simbolo_t*)$2)->userType = $1;
  //} else {
  //  yyerror("ERRO: Tipo de usuario inexistente");
  //}
};




/** Comandos simples **/

/* Declaração de variável */
DECL_VAR_LOCAL: DECL_TIPO_CONST_STATIC TK_IDENTIFICADOR
{
  if(search_stack_local(stack, ((simbolo_t*)$2)->lexema, 6)!=NULL){
    if (((simbolo_t*)$2)->varType > 0){
      printf("\nVariable %s declared, line %d\n", ((simbolo_t*)$2)->lexema, myLineNumber);
      exit(IKS_ERROR_DECLARED);
    }
  }

  ((simbolo_t*)$2)->varType = $1;
  ((simbolo_t*)$2)->dictType = DICT_TYPE_VAR;
  $$ = NULL;

  switch($1){
    case SIMBOLO_LITERAL_INT:
      ((simbolo_t*)$2)->size = 4;
      break;
    case SIMBOLO_LITERAL_FLOAT:
      ((simbolo_t*)$2)->size = 8;
      break;
    case SIMBOLO_LITERAL_CHAR:
      ((simbolo_t*)$2)->size = 1;
      break;
    case SIMBOLO_LITERAL_STRING:
      ((simbolo_t*)$2)->size = sizeof(((simbolo_t*)$2)->value.s);
      break;
    case SIMBOLO_LITERAL_BOOL:
      ((simbolo_t*)$2)->size = 1;
      break;
  }

  ((simbolo_t*)$2)->escopo = 1; //fp pq estamos em função
  ((simbolo_t*)$2)->desloc = fp;

  fp += ((simbolo_t*)$2)->size;
};

DECL_VAR_LOCAL: DECL_TIPO_CONST_STATIC TK_IDENTIFICADOR '[' LISTA_DE_DIMENSOES ']'
{
  if(search_stack_local(stack, ((simbolo_t*)$2)->lexema, 6)!=NULL){
    if (((simbolo_t*)$2)->varType > 0){
      printf("\nVariable %s declared, line %d\n", ((simbolo_t*)$2)->lexema, myLineNumber);
      exit(IKS_ERROR_DECLARED);
    }
  }

  ((simbolo_t*)$2)->varType = $1;
  ((simbolo_t*)$2)->dictType = DICT_TYPE_VEC;
  $$ = NULL;

  switch($1){
    case SIMBOLO_LITERAL_INT:
      ((simbolo_t*)$2)->size = 4 * $4;
      break;
    case SIMBOLO_LITERAL_FLOAT:
      ((simbolo_t*)$2)->size = 8 * $4;
      break;
    case SIMBOLO_LITERAL_CHAR:
      ((simbolo_t*)$2)->size = 1 * $4;
      break;
    case SIMBOLO_LITERAL_STRING:
      ((simbolo_t*)$2)->size = sizeof(((simbolo_t*)$2)->value.s) * $4;
      break;
    case SIMBOLO_LITERAL_BOOL:
      ((simbolo_t*)$2)->size = 1 * $4;
      break;
  }

  ((simbolo_t*)$2)->escopo = 1; //fp pq estamos em função
  ((simbolo_t*)$2)->desloc = fp;

  fp += ((simbolo_t*)$2)->size;
};

DECL_VAR_LOCAL: DECL_TIPO_CONST_STATIC TK_IDENTIFICADOR TK_OC_LE TK_IDENTIFICADOR
{
  if(search_stack_local(stack, ((simbolo_t*)$2)->lexema, 6)!=NULL){
    if (((simbolo_t*)$2)->varType > 0){
      printf("\nVariable %s declared, line %d\n", ((simbolo_t*)$2)->lexema, myLineNumber);
      exit(IKS_ERROR_DECLARED);
    }
  }

  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_ATRIBUICAO;
  node->simbolo = $3;
  ast_nodo_t* node2 = malloc(sizeof(ast_nodo_t));
  node2->tipoNodo = AST_IDENTIFICADOR;
  node2->simbolo = $2;
  ast_nodo_t* node3 = malloc(sizeof(ast_nodo_t));
  node3->tipoNodo = AST_IDENTIFICADOR;
  node3->simbolo = $4;
  $$ = tree_make_binary_node(node, tree_make_node(node2), tree_make_node(node3));
  ((simbolo_t*)$2)->varType = $1;
  ((simbolo_t*)$2)->dictType = DICT_TYPE_VAR;
};
DECL_VAR_LOCAL: DECL_TIPO_CONST_STATIC TK_IDENTIFICADOR TK_OC_LE LITERAL
{
  if(search_stack_local(stack, ((simbolo_t*)$2)->lexema, 6)!=NULL){
    if (((simbolo_t*)$2)->varType > 0){
      printf("\nVariable %s declared, line %d\n", ((simbolo_t*)$2)->lexema, myLineNumber);
      exit(IKS_ERROR_DECLARED);
    }
  }


  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_ATRIBUICAO;
  node->simbolo = $3;
  ast_nodo_t* node2 = malloc(sizeof(ast_nodo_t));
  node2->tipoNodo = AST_IDENTIFICADOR;
  node2->simbolo = $2;
  $$ = tree_make_binary_node(node, tree_make_node(node2), $4);
  ((simbolo_t*)$2)->varType = $1;
  ((simbolo_t*)$2)->dictType = DICT_TYPE_VAR;
};

VETOR_INDEXADO: TK_IDENTIFICADOR '[' EXPRESSAO ']'
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_IDENTIFICADOR;
  node->simbolo = $1;
  ast_nodo_t* node2 = malloc(sizeof(ast_nodo_t));
  node2->simbolo = $1;
  node2->tipoNodo = AST_VETOR_INDEXADO;
  $$ = tree_make_binary_node(node2, tree_make_node(node), $3);

  if((((ast_nodo_t*)$3->value)->simbolo->type) == SIMBOLO_LITERAL_CHAR){
    printf("\nCan not use char, line %d\n", myLineNumber);
     exit(IKS_ERROR_CHAR_TO_X);
  }
  if((((ast_nodo_t*)$3->value)->simbolo->type) == SIMBOLO_LITERAL_STRING){
    printf("\nCan not use string, line %d\n", myLineNumber);
     exit(IKS_ERROR_STRING_TO_X);
  }

  comp_dict_item_t* original = search_stack(stack, node->simbolo->lexema, 6);
  if(original==NULL)
  {
     printf("\nVector %s not declared, line %d\n", node->simbolo->lexema, myLineNumber);
     exit(IKS_ERROR_UNDECLARED);
  }

  if( ((simbolo_t*)original->value)->dictType == DICT_TYPE_FUNC ){
    printf("\nIdentifier should be used as function, line %d\n", myLineNumber);
    exit(IKS_ERROR_FUNCTION);
  }

  if( ((simbolo_t*)original->value)->dictType == DICT_TYPE_VAR ){
    printf("\nIdentifier should be used as variable, line %d\n", myLineNumber);
    exit(IKS_ERROR_VARIABLE);
  }

  int sizeTipo = size_tipo(((simbolo_t*)original->value)->varType);

//printf("----------- %d -------------", sizeTipo);

  if (((ast_nodo_t*)$3->value)->simbolo->varType == SIMBOLO_LITERAL_INT)
    if((((simbolo_t*)original->value)->size)/sizeTipo < atoi(((ast_nodo_t*)$3->value)->simbolo->lexema))
    {
       printf("\nVector position inexistent (vector size = %d < given position = %d), line %d\n", (((simbolo_t*)original->value)->size)/sizeTipo, atoi(((ast_nodo_t*)$3->value)->simbolo->lexema), myLineNumber);
       exit(IKS_ERROR_VECTOR);
    }

};

/* Atribuição */
ATRIBUICAO: TK_IDENTIFICADOR '=' EXPRESSAO
{


  comp_dict_item_t* original = search_stack(stack, ((simbolo_t*)$1)->lexema, 6);
  if(original==NULL)
  {
     printf("\nVariable %s not declared, line %d\n", ((simbolo_t*)$1)->lexema, myLineNumber);
     exit(IKS_ERROR_UNDECLARED);
  }

  if(((simbolo_t*)original->value)->dictType == DICT_TYPE_VEC){
    printf("Identifier should be used as vector, line %d\n", myLineNumber);
    exit(IKS_ERROR_VECTOR);
  }

  if(((simbolo_t*)original->value)->dictType == DICT_TYPE_FUNC){
    printf("Identifier should be used as function, line %d\n", myLineNumber);
    exit(IKS_ERROR_FUNCTION);
  }
  //printf("***%d***", ((ast_nodo_t*)$3->value)->simbolo->type);

  check_type(((simbolo_t*)original->value)->lexema, ((simbolo_t*)original->value)->varType, ((ast_nodo_t*)$3->value)->simbolo->type, ((ast_nodo_t*)$3->value)->simbolo, 0);

  //printf("---%s---%d---%d---\n", ((simbolo_t*)$1)->lexema, ((simbolo_t*)original->value)->varType, ((ast_nodo_t*)$3->value)->simbolo->type);

  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_IDENTIFICADOR;
  node->simbolo = (simbolo_t*)original->value;
  ast_nodo_t* node2 = malloc(sizeof(ast_nodo_t));
  node2->tipoNodo = AST_ATRIBUICAO;
  $$ = tree_make_binary_node(node2, tree_make_node(node), $3);
};

ATRIBUICAO: VETOR_INDEXADO '=' EXPRESSAO
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_ATRIBUICAO;
  $$ = tree_make_binary_node(node, $1, $3);

  comp_dict_item_t* original = search_stack(stack, ((ast_nodo_t*)$1->value)->simbolo->lexema, 6);

  check_type(((simbolo_t*)original->value)->lexema, ((simbolo_t*)original->value)->varType, ((ast_nodo_t*)$3->value)->simbolo->type, ((ast_nodo_t*)$3->value)->simbolo, 1);

  ((ast_nodo_t*)$1->first->value)->simbolo = (simbolo_t*)original->value;
};
ATRIBUICAO: TK_IDENTIFICADOR '!' TK_IDENTIFICADOR '=' EXPRESSAO
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_ATRIBUICAO;
  ast_nodo_t* nodeId1 = malloc(sizeof(ast_nodo_t));
  nodeId1->tipoNodo = AST_IDENTIFICADOR;
  nodeId1->simbolo = $1;
  ast_nodo_t* nodeId2 = malloc(sizeof(ast_nodo_t));
  nodeId2->tipoNodo = AST_IDENTIFICADOR;
  nodeId2->simbolo = $3;
  $$ = tree_make_ternary_node(node, tree_make_node(nodeId1), tree_make_node(nodeId2), $5);
};

/* Entrada e Saida */
INPUT: TK_PR_INPUT EXPRESSAO
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_INPUT;
  $$ = tree_make_unary_node(node, $2);
};
OUTPUT: TK_PR_OUTPUT EXPRESSAO
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_OUTPUT;
  $$ = tree_make_unary_node(node, $2);
};
OUTPUTS: TK_PR_OUTPUT LISTA_EXPRESSAO
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_OUTPUT;
  $$ = tree_make_unary_node(node, $2);
};
LISTA_EXPRESSAO: EXPRESSAO { $$ = $1;};
LISTA_EXPRESSAO: EXPRESSAO ',' LISTA_EXPRESSAO
{
  tree_insert_node($1, $3);
  $$ = $1;
};

/* Chamada de Função */
CHAMADA_DE_FUNCAO: TK_IDENTIFICADOR '(' ')'
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_IDENTIFICADOR;
  node->simbolo = $1;
  ast_nodo_t* node2 = malloc(sizeof(ast_nodo_t));
  node2->tipoNodo = AST_CHAMADA_DE_FUNCAO;
  node2->simbolo = $1;
  $$ = tree_make_unary_node(node2, tree_make_node(node));

  comp_dict_item_t* original = search_stack(stack, node->simbolo->lexema, 6);
  if(original==NULL)
  {
     printf("\nFunction %s not declared, line %d\n", node->simbolo->lexema, myLineNumber);
     exit(IKS_ERROR_UNDECLARED);
  }


  if( ((simbolo_t*)original->value)->dictType == DICT_TYPE_VAR ){
    printf("\nIdentifier should be used as variable, line %d\n", myLineNumber);
    exit(IKS_ERROR_VARIABLE);
  }
  if( ((simbolo_t*)original->value)->dictType == DICT_TYPE_VEC ){
    printf("\nIdentifier should be used as vector, line %d\n", myLineNumber);
    exit(IKS_ERROR_VECTOR);
  }

  if (((simbolo_t*)original->value)->parameters->bottom!=NULL){
    printf("\nMissing args, line %d\n", myLineNumber);
    exit(IKS_ERROR_MISSING_ARGS);
  }
};
CHAMADA_DE_FUNCAO: TK_IDENTIFICADOR '(' LISTA_EXPRESSAO ')'
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_IDENTIFICADOR;
  node->simbolo = $1;
  ast_nodo_t* node2 = malloc(sizeof(ast_nodo_t));
  node2->tipoNodo = AST_CHAMADA_DE_FUNCAO;
  node2->simbolo = $1;
  $$ = tree_make_binary_node(node2, tree_make_node(node), $3);

  comp_dict_item_t* original = search_stack(stack, node->simbolo->lexema, 6);
  if(original==NULL)
  {
     printf("\nFunction %s not declared, line %d\n", node->simbolo->lexema, myLineNumber);
     exit(IKS_ERROR_UNDECLARED);
  }

  if( ((simbolo_t*)original->value)->dictType == DICT_TYPE_VAR ){
    printf("\nIdentifier should be used as variable, line %d\n", myLineNumber);
    exit(IKS_ERROR_VARIABLE);
  }
  if( ((simbolo_t*)original->value)->dictType == DICT_TYPE_VEC ){
    printf("\nIdentifier should be used as vector, line %d\n", myLineNumber);
    exit(IKS_ERROR_VECTOR);
  }

 comp_tree_t *treeArgs = $3;
 comp_stack_t *firstType = ((simbolo_t*)original->value)->parameters->bottom;

 //tree_debug_print(treeArgs);

 do {
  if (treeArgs != NULL && firstType == NULL){
    printf("\nToo much args, line %d\n", myLineNumber);
    exit(IKS_ERROR_EXCESS_ARGS);
  }
  if (treeArgs == NULL && firstType != NULL){
    printf("\nMissing args, line %d\n", myLineNumber);
    exit(IKS_ERROR_MISSING_ARGS);
  }

  check_type(((ast_nodo_t*)treeArgs->value)->simbolo->lexema, ((ast_nodo_t*)treeArgs->value)->simbolo->varType, *((int*)firstType->value), ((ast_nodo_t*)$3->value)->simbolo, 2);

  //printf("Given type: %d %s\n", ((ast_nodo_t*)treeArgs->value)->simbolo->varType, ((ast_nodo_t*)treeArgs->value)->simbolo->lexema);
  //printf("Requi type: %d\n", *((int*)firstType->value));

  treeArgs = treeArgs->first;
  firstType = firstType->next;
 } while (treeArgs != NULL);

 if (treeArgs != NULL && firstType == NULL){
   printf("\nToo much args, line %d\n", myLineNumber);
   exit(IKS_ERROR_EXCESS_ARGS);
 }
 if (treeArgs == NULL && firstType != NULL){
   printf("\nMissing args, line %d\n", myLineNumber);
   exit(IKS_ERROR_MISSING_ARGS);
 }

};

/* Shift */
SHIFT: TK_IDENTIFICADOR TK_OC_SL TK_LIT_INT;
SHIFT: TK_IDENTIFICADOR TK_OC_SR TK_LIT_INT;

/* Retorno, Break, Continue */
CONTROLE: TK_PR_RETURN EXPRESSAO
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_RETURN;
  $$ = tree_make_unary_node(node, $2);

  int type_test = ((ast_nodo_t*)$2->value)->simbolo->type;

  if(type_test == 6)
  	type_test = ((simbolo_t*) search_stack(stack, ((ast_nodo_t*)$2->value)->simbolo->lexema, 6)->value)->varType;

  if((func_type==SIMBOLO_LITERAL_CHAR || func_type==SIMBOLO_LITERAL_STRING || type_test==SIMBOLO_LITERAL_CHAR || type_test==SIMBOLO_LITERAL_STRING) && func_type != type_test)
	exit(IKS_ERROR_WRONG_PAR_RETURN);
};
CONTROLE: TK_PR_BREAK { $$ = NULL;};
CONTROLE: TK_PR_CONTINUE { $$ = NULL; };

CASE: TK_PR_CASE TK_LIT_INT;

/* Controle de Fluxo */
CONTROLE_DE_FLUXO: TK_PR_IF '(' EXPRESSAO ')' TK_PR_THEN programaFluxo
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_IF_ELSE;
  if ($6 != NULL)
    $$ = tree_make_binary_node(node, $3, $6);
  else
    $$ = tree_make_unary_node(node, $3);
};
CONTROLE_DE_FLUXO: TK_PR_IF '(' EXPRESSAO ')' TK_PR_THEN programaFluxo TK_PR_ELSE programaFluxo
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_IF_ELSE;
  node->isIfWithElse = 1;
  if ($6 != NULL){
    if ($8 != NULL)
      $$ = tree_make_ternary_node(node, $3, $6, $8);
    else
      $$ = tree_make_binary_node(node, $3, $6);
  }
  else if ($8 != NULL)
    $$ = tree_make_binary_node(node, $3, $8);
  else
    $$ = tree_make_unary_node(node, $3);
};

CONTROLE_DE_FLUXO: TK_PR_FOREACH '(' TK_IDENTIFICADOR ':' LISTA_EXPRESSAO ')' programaFluxo
{ $$ = NULL; };

CONTROLE_DE_FLUXO: TK_PR_FOR '(' LISTA_COMANDOS ':' EXPRESSAO ':' LISTA_COMANDOS ')' programaFluxo
{ $$ = NULL; };
LISTA_COMANDOS: comandoLocalFor;
LISTA_COMANDOS: comandoLocalFor ',' LISTA_COMANDOS;

CONTROLE_DE_FLUXO: TK_PR_WHILE '(' EXPRESSAO ')' TK_PR_DO programaFluxo
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_WHILE_DO;
  if ($6 != NULL)
    $$ = tree_make_binary_node(node, $6, $3);
  else
    $$ = tree_make_unary_node(node, $3);
};

CONTROLE_DE_FLUXO: TK_PR_DO programaFluxo TK_PR_WHILE '(' EXPRESSAO ')'
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_DO_WHILE;
  if ($2 != NULL)
    $$ = tree_make_binary_node(node, $2, $5);
  else
    $$ = tree_make_unary_node(node, $5);
};

CONTROLE_DE_FLUXO: TK_PR_SWITCH '(' EXPRESSAO ')' programaFluxo
{ $$ = NULL; };


/** Expressões **/
EXPRESSAO: EXPRESSAO_LOGICA {$$ = $1;};
EXPRESSAO: EXPRESSAO_ARITMETICA {$$ = $1;};

//regra util depois (retorna soh bool)
EXPRESSAO_LOGICA: OP_LOGICA_ENCADEADAS {$$ = $1;};;
//regra util depois (retorna soh int)
EXPRESSAO_ARITMETICA: OP_ARITMETICA {$$ = $1;};

OPERADOR_ARITMETICO_1: '*' {
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_ARIM_MULTIPLICACAO;
  $$ = tree_make_node(node);
};
OPERADOR_ARITMETICO_1: '/' {
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_ARIM_DIVISAO;
  $$ = tree_make_node(node);
};
OPERADOR_ARITMETICO_2: '+' {
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_ARIM_SOMA;
  $$ = tree_make_node(node);
};
OPERADOR_ARITMETICO_2: '-' {
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_ARIM_SUBTRACAO;
  $$ = tree_make_node(node);
};
OPERADOR_RELACIONAL: OPERADOR_IGUALDADE | OPERADOR_DIFERENCA;
OPERADOR_DIFERENCA: TK_OC_LE {
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LOGICO_COMP_LE;
  $$ = tree_make_node(node);  };
OPERADOR_DIFERENCA: TK_OC_GE {
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LOGICO_COMP_GE;
  $$ = tree_make_node(node);  };
OPERADOR_IGUALDADE: TK_OC_EQ {
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LOGICO_COMP_IGUAL;
  $$ = tree_make_node(node);  };
OPERADOR_IGUALDADE: TK_OC_NE {
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LOGICO_COMP_DIF;
  $$ = tree_make_node(node);  };
OPERADOR_DIFERENCA: '<'{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LOGICO_COMP_L;
  $$ = tree_make_node(node);  };
OPERADOR_DIFERENCA: '>'{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LOGICO_COMP_G;
  $$ = tree_make_node(node);  };

OPERADOR_LOGICO: TK_OC_AND{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LOGICO_E;
  $$ = tree_make_node(node);  };
OPERADOR_LOGICO: TK_OC_OR{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LOGICO_OU;
  $$ = tree_make_node(node);  };

OP_ARITMETICA: E {$$ = $1;};

E: E OPERADOR_ARITMETICO_2 T{
  $$ = $2;
  parse_aritmetic((ast_nodo_t*)$1->value, (ast_nodo_t*)$$->value, (ast_nodo_t*)$3->value);
  tree_insert_node($$, $1);
  tree_insert_node($$, $3);
};
E: T {$$ = $1;};

T: T OPERADOR_ARITMETICO_1 F{
  $$ = $2;
  parse_aritmetic((ast_nodo_t*)$1->value, (ast_nodo_t*)$$->value, (ast_nodo_t*)$3->value);
  tree_insert_node($$, $1);
  tree_insert_node($$, $3);
};
T: F {$$ = $1;};

F: '(' E ')' {$$ = $2;};
F: FATOR {$$ = $1;};


//TODO deveria ter mas causa conflitos
//OP_LOGICA: FATOR {$$ = $1;};
/*
BOOL: TK_LIT_TRUE
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  $$ = tree_make_node(node);
};
BOOL: TK_LIT_FALSE
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  $$ = tree_make_node(node);
};

TERMO: TK_IDENTIFICADOR {
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  $$ = tree_make_node(node);
};
TERMO: BOOL {$$ = $1;};
TERMO: '(' TERMO ')' {$$ = $2;};

OP_LOGICA: BOOL {$$ = $1;};
//conflito
/*OP_LOGICA: TK_IDENTIFICADOR OPERADOR_IGUALDADE BOOL {
  $$ = $2;
  tree_insert_node($$, $1);
  tree_insert_node($$, $3);
};
OP_LOGICA: BOOL OPERADOR_IGUALDADE TERMO {
  $$ = $2;
  tree_insert_node($$, $1);
  tree_insert_node($$, $3);
};*/

OP_LOGICA: OP_ARITMETICA OPERADOR_RELACIONAL OP_ARITMETICA
{
  $$ = $2;
  tree_insert_node($$, $1);
  tree_insert_node($$, $3);
};
OP_LOGICA: '(' OP_LOGICA ')' { $$ = $2;};

OP_LOGICA_ENCADEADAS: OP_LOGICA { $$ = $1;};
OP_LOGICA_ENCADEADAS: OP_LOGICA OPERADOR_LOGICO OP_LOGICA_ENCADEADAS
{
  $$ = $2;
  tree_insert_node($$, $1);
  tree_insert_node($$, $3);
};
OP_LOGICA_ENCADEADAS: '(' OP_LOGICA OPERADOR_LOGICO OP_LOGICA_ENCADEADAS ')'
{
  $$ = $3;
  tree_insert_node($$, $2);
  tree_insert_node($$, $4);
};

FATOR: TK_IDENTIFICADOR {
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_IDENTIFICADOR;

  comp_dict_item_t* original = search_stack(stack, ((simbolo_t*)$1)->lexema, 6);
  if(original==NULL)
  {
     printf("\nVariable \"%s\" not declared, line %d\n", ((simbolo_t*)$1)->lexema, myLineNumber);
    exit(IKS_ERROR_DECLARED);
  }

  node->simbolo = (simbolo_t*)original->value;
  $$ = tree_make_node(node);

};

FATOR: TK_LIT_INT {
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  node->simbolo->varType = SIMBOLO_LITERAL_INT;
  $$ = tree_make_node(node);
};
FATOR: TK_LIT_FLOAT{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  node->simbolo->varType = SIMBOLO_LITERAL_FLOAT;
  $$ = tree_make_node(node);
};
FATOR: '-' TK_LIT_INT {
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $2;
  node->simbolo->varType = SIMBOLO_LITERAL_INT;

  comp_tree_t* arv = tree_make_node(node);

  ast_nodo_t* nodeMenos = malloc(sizeof(ast_nodo_t));
  nodeMenos->tipoNodo = AST_ARIM_INVERSAO;

  $$ = tree_make_node(nodeMenos);
  tree_insert_node($$, arv);
};
FATOR: '-' TK_LIT_FLOAT{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $2;
  node->simbolo->varType = SIMBOLO_LITERAL_FLOAT;

  comp_tree_t* arv = tree_make_node(node);

  ast_nodo_t* nodeMenos = malloc(sizeof(ast_nodo_t));
  nodeMenos->tipoNodo = AST_ARIM_SUBTRACAO;

  $$ = tree_make_node(nodeMenos);
  tree_insert_node($$, arv);
};

FATOR: TK_LIT_TRUE
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  node->simbolo->varType = SIMBOLO_LITERAL_BOOL;
  $$ = tree_make_node(node);
};
FATOR: TK_LIT_FALSE
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  node->simbolo->varType = SIMBOLO_LITERAL_BOOL;
  $$ = tree_make_node(node);
};
FATOR: TK_LIT_CHAR
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  node->simbolo->varType = SIMBOLO_LITERAL_CHAR;
  $$ = tree_make_node(node);
};
FATOR: TK_LIT_STRING
{
  ast_nodo_t* node = malloc(sizeof(ast_nodo_t));
  node->tipoNodo = AST_LITERAL;
  node->simbolo = $1;
  node->simbolo->varType = SIMBOLO_LITERAL_STRING;
  $$ = tree_make_node(node);
};

FATOR: CHAMADA_DE_FUNCAO { $$ = $1; };
FATOR: VETOR_INDEXADO { $$ = $1; };



%%
